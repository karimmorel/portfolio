$(window).ready(function() {

    function t() {
        var t = $(this).scrollTop();
        s >= t ? s = t : t <= hauteur ? $("nav").css({
            top: -500
        }) : t >= s && ($("nav").css({
            top: -500
        }), s = t)
    }

    function e() {
        var t = $(this).scrollTop();
        largeur > 900 ? t > window.innerHeight / .9 ? $("#intro").css({
            opacity: 0
        }) : t < window.innerHeight / .9 && $("#intro").css({
            opacity: 1
        }) : t > window.innerHeight / .8 ? $("#intro").css({
            opacity: 0
        }) : t < window.innerHeight / .8 && $("#intro").css({
            opacity: 1
        })
    }

    function i(t, e, i, n) {
        this.width = n, this.height = n, this.element = document.createElement("canvas"), $(this.element).attr("id", e).attr("class", i).width(this.width).height(this.height).appendTo(t), this.context = this.element.getContext("2d")
    }

    function n(t, e) {
        return Math.floor((e - t) * Math.random()) + t
    }

    function o() {
        var t = ($(window).width() - 40) / 1,
            e = 20;
        for ($irg = 0; $irg < 2; $irg++) $("#lgrille" + $irg).css({
            left: e
        }), e += t
    }

    function r(t, e, i, n) {
        e = F[Math.floor(Math.random() * F.length)], e == i && (e = F[Math.floor(Math.random() * F.length)]), i = [];
        var o = setInterval(function() {
            i += e[n], $("#containfooter h3").html(i), n++, n >= e.length && (clearInterval(o), setTimeout(function() {
                a(40, e, i, 0), 1e3
            }, 3e3))
        }, t)
    }

    function a(t, e, i, n) {
        var o = setInterval(function() {
            n++, $("#containfooter h3").html(e.slice(0, -n)), n >= e.length && (clearInterval(o), r(40, W, i, 0))
        }, t)
    }
    $(window).scroll(t), $(window).scroll(e);
    var s = 100;
    if ($i = 0, window.innerWidth > 900 ? $("#loadanimcontent").css({
            "margin-top": window.innerHeight / 3.5
        }) : $("#loadanimcontent").css({
            "margin-top": window.innerHeight / 5
        }), $(".loader").css({
            height: window.height
        }), $("#quitnav").click(function() {
            $("nav").css({
                top: -500
            })
        }), hauteur = window.innerHeight, largeur = window.innerWidth, $("nav").css({
            top: -500
        }), largeur < 900 ? ($("#top").css({
            height: hauteur
        }), $("#topimg").css({
            "padding-top": "0",
            height: hauteur
        })) : ($("#top").css({
            height: hauteur
        }), $("#topimg").css({
            "padding-top": hauteur / 3.5
        })), $(window).resize(function() {
            hauteur = window.innerHeight, largeur = window.innerWidth, $("nav").css({
                top: -500
            }), 1 == $i && ($("#topimg").css({
                left: (largeur - 300) / 2,
                height: "140px"
            }), $("#topimg").css({
                top: hauteur / 2.5
            }))
        }), largeur > 900) var h = n(Math.floor(window.innerWidth / 30), Math.floor(window.innerWidth / 36));
    else if (largeur < 900) var h = n(5, 7);
    for (var l = 0, c = ["#f5f6fa"], d = 0; h > d; d++) {
        var g = c[Math.floor(Math.random() * c.length)],
            u = n(0, $(window).width()),
            m = n(-100, -160),
            w = (n(18, 550), n(1e4, 16e3), n(0, 16e3));
        l++;
        var f = l,
            p = {},
            v = n(7, 22);
        p["forme" + f] = new i("#top", "forme" + f, "formes " + g, v);
        var M = document.getElementById("forme" + f),
            y = M.getContext("2d"),
            b = M.width / 2,
            x = M.height / 2,
            E = 700;
        y.beginPath(), y.arc(b, x, E, 0, 2 * Math.PI, !1), y.fillStyle = g, y.fill(), $("#forme" + f).css({
            right: u,
            bottom: m,
            "animation-duration": 1 / v * 150000 + "ms",
            "animation-delay": w + "ms",
            "border-radius": n(40, 100)
        }), console.log($("#forme" + f).height)
    }
    for (var d = 0; h > d; d++) {
        var g = c[Math.floor(Math.random() * c.length)],
            u = n(0, $(window).width()),
            m = n(-100, -160),
            w = (n(18, 550), n(1e4, 16e3), n(0, 16e3));
        l++;
        var f = l,
            p = {},
            v = n(7, 22);
        p["forme" + f] = new i("#top", "forme" + f, "formes " + g, v);
        var M = document.getElementById("forme" + f),
            y = M.getContext("2d"),
            b = M.width / 2,
            x = M.height / 2,
            E = 700;
        y.beginPath(), y.arc(b, x, E, 0, 2 * Math.PI, !1), y.fillStyle = g, y.fill(), $("#forme" + f).css({
            left: u,
            bottom: m,
            "animation-duration": 1 / v * 150000 + "ms",
            "animation-delay": w + "ms",
            "border-radius": n(40, 100)
        })
    }
    var I = 0,
        P = 160,
        H = 1;
    $("#cercletop h1").css({
        "margin-top": P / 2.5
    }), $(".formes").mouseover(function() {
        $(this).css({
            border: "20px solid transparent"
        }), $(this).hide(140), $(".cls-1").css({
            transform: "translate(600 200)"
        }), I++, 2 > I ? $("#intro h2").html(I + " bubble.") : $("#intro h2").html(I + " bubbles.");
        var t = $(this).attr("id");
        if (setTimeout(function(e) {
                $("#" + t).css({
                    border: "inherit"
                }), $("#" + t).show("slow")
            }, n(17e3, 1)), H++, 2 == H) {
            var e = ["Humble", "Genius", "Perfect", "Great", "Creative", "Shy", "English speaking", "French speaking", "Positive", "The best", "The only one", "Generous"],
                i = n(0, e.length);
            $("#cercletop h1").html(e[i]), H = 0
        }
    });
    var T = 2,
        C = ($(window).width() - 40) / (T - 1),
        D = 20;
    for ($g = 0; $g < T; $g++) $("#content").append('<div class="grille lgrille" id="lgrille' + $g + '"></div>'), $("#lgrille" + $g).css({
        left: D
    }), D += C;
    $(window).resize(o);
    var F = ["Contact me!", "Have a look at my CV", "I love Indonesia", "I would love to work with you!", "Do you know \"Saucisson\"?", "Thank you for visiting my website", "Have a great day :)", "Do not eat to much chocolate"],
        W = F[Math.floor(Math.random() * F.length)],
        z = Array();
    $("#containfooter h3").html(W), r(50, W, z, 0)
});

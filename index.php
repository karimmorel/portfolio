<!DOCTYPE html>
<html lang="fr">
<head>
  <!-- Global Site Tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-84217002-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-84217002-1');
  </script>
  <meta charset="UTF-8"/>
  <title>Karim Morel | Développeur Web</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="icon" type="image/png" href="img/favicon.ico" />

  <meta name="author" content="Karim Morel - Développeur web" />
  <meta name="copyright" content="Morel Karim" />
  <meta name="creation_Date" content="12-09-2016" />

  <meta name="description" content="Développeur web freelance. Je m'occupe du développement Wordpress / PHP de votre site Internet et de son optimisation mobile et SEO." />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link rel="alternate" href="http://karimmorel.fr/" hreflang="fr" />

  <!-- SOCIAL -->

  <!-- Social: Twitter -->
  <meta name="twitter:card" content="Karim Morel">
  <meta name="twitter:site" content="@MorelKarim">
  <meta name="twitter:creator" content="@MorelKarim">
  <meta name="twitter:title" content="Karim Morel - Developpeur Web Créatif">
  <meta name="twitter:description" content="Vous recherchez un développeur web ? Vous avez enfin trouvé la solution !">
  <meta name="twitter:image:src" content="http://www.karimmorel.com/img/fond.jpg">

  <!-- Social: Facebook / Open Graph -->
  <meta property="fb:admins" content="karimmorel.dev">
  <meta property="og:url" content="http://www.karimmorel.fr">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Karim Morel - Developpeur Web Créatif">
  <meta property="og:image" content="http://www.karimmorel.com/img/fond.jpg" />
  <meta property="og:description" content="Vous recherchez un développeur web ? Vous avez enfin trouvé la solution !">
  <meta property="og:site_name" content="Karim Morel">

  <!-- FONT -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans|Quicksand&display=swap" rel="stylesheet">
  <style>
  html
  {
    background:rgb(251,252,249);
  }

  body
  {
    max-width:100%;
    width:100%;
    margin-left:auto;
    margin-right:auto;
    background:rgb(21,22,23);
  }

  .loader
  {
    position:fixed;
    background:rgb(252,252,252);
    width:100%;
    height:1200px;
    top:0px;
    z-index:21;
  }
  .loader #loadanimcontent
  {
    height:500px;
    padding-top:30px;
    width:30%;
    max-width:900px;
    margin-left:auto;
    margin-right:auto;
    position:relative;
    text-align:center;
  }
  .loader p
  {
    color:rgb(21,22,23);
    font-size: 18px;
    font-family:'Roboto', sans-serif;
    text-align:center;
    font-weight:300;
    line-height:27px;
  }
  .loader .anim,
  .loader .anim:before,
  .loader .anim:after {
    border-radius: 50%;
  }
  .loader .anim:before,
  .loader .anim:after {
    position: absolute;
    content: '';
  }
  #loadanimcontent video
  {
    margin-left:auto;
    margin-right:auto;
    display:block;
  }
  .loader .anim:before {
    width: 5.2em;
    height: 10.2em;
    background: rgb(251,251,251);
    border-radius: 10.2em 0 0 10.2em;
    top: -0.1em;
    left: -0.1em;
    -webkit-transform-origin: 5.2em 5.1em;
    transform-origin: 5.2em 5.1em;
    -webkit-animation: load2 2s infinite ease 1.5s;
    animation: load2 2s infinite ease 1.5s;
  }
  .loader .anim {
    color: rgb(0,180,207);
    font-size: 11px;
    text-indent: -99999em;
    margin: 55px auto;
    position: relative;
    width: 10em;
    height: 10em;
    box-shadow: inset 0 0 0 1em;
    -webkit-transform: translateZ(0);
    -ms-transform: translateZ(0);
    transform: translateZ(0);
  }
  .loader .anim:after {
    width: 6.2em;
    height: 11.2em;
    background: rgb(251,251,251);
    border-radius: 0 10.2em 10.2em 0;
    top: -0.1em;
    left: 5.1em;
    -webkit-transform-origin: 0px 5.1em;
    transform-origin: 0px 5.1em;
    -webkit-animation: load2 2s infinite ease;
    animation: load2 2s infinite ease;
  }

  #top
  {
    padding:0px;
    position:relative;
    background-size:auto;
    width:100%;
    margin-top:0px;
    box-sizing: border-box;
    background:black;
    z-index:1;
  }
  #cercletop
  {
    width:100%;
    border-radius:100%;
    margin-left:auto;
    margin-right:auto;
    position:fixed;
    top:50%;
    margin-top:-212px;
    text-align:center;
    pointer-events: none;
  }
  #cercletop h1
  {
    font-family: 'Roboto', sans-serif;
    color:rgb(255,255,255);
    letter-spacing:1.6px;
    margin-left:auto;
    margin-right:auto;
    text-shadow: rgba(0,0,0,.1) 0 17px 20px;
    font-size:6rem;
    font-weight:300;
    font-style:italic;
    z-index:2;
  }
  #cercletop p
  {
    font-family:'Roboto',sans-serif;
    color:#fff;
    font-size:2rem;
    font-weight:300;
    margin-top:20px;
  }
  #contentmenubutton
  {
    width:100%;
    height:50px;
    position:fixed;
    top:0;
    z-index:20;
    pointer-events: none;
  }
  #menubutton
  {
    width:50px;
    height:50px;
    color:rgba(25,25,25,.9);
    background:rgba(251,251,251,1);
    border:1px solid rgb(241,241,241);
    margin-top:0px;
    margin-left:auto;
    margin-right:auto;
    text-align:center;
    border-radius:0 0 3px 3px;
    cursor:pointer;
    box-shadow: 0 4px 6px -6px;
    pointer-events:auto;
  }
  #menubutton a
  {
    line-height:50px;
    text-decoration:none;
    color:black;
    font-family:'Roboto',sans-serif;
    font-size:16px;
    letter-spacing:1.2px;
    font-weight:300;
  }
  .formes{position:fixed;box-sizing:border-box;border:0;z-index:-1;opacity:.5;box-shadow:0 15px 10px rgba(0,0,0,.07);background-attachment:fixed;animation-name:levitation;animation-iteration-count:infinite;animation-timing-function:linear}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84217002-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
 <!--  PAS BESOIN DE METTRE DE CV POUR LE MOMENT.   <div id="contentmenubutton"><div id="menubutton"><a href="assets/cv.pdf" target="_blank">CV</a></div></div> -->

 <div class="loader"><div id="loadanimcontent"><div class="anim"></div><video loop autoplay muted>
   <source src="img/test.mp4" type="video/mp4">
     Your navigator do not support this video format.
   </video><p>Loading.</p></div>
 </div>
 <div id="top">
   <div id="cercletop">
     <h1><div class="glitch-wrapper">
  <div class="glitch" data-text="KARIM MOREL">KARIM MOREL</div>
</div></h1>
     <div class="sep"></div>
     <p>Web Developer</p>
   </div>
 </div>
 <div id="content">
   <div id="intro">
     <h2>Chase bubbles</h2>
     <div class="sep"></div>
   </div>
   <div class="postintro">
     <h3>Looking for new opportunities</h3>
     <p>After 3 years working in France, I give myself a new challenge.<br/>I would like to bring my knowledge and my skills in an english speaking team, in a project where I can integrate a positive and creative team.</p>
   </div>
   


 <div id="contentw">
     <div class="contentwork">
      <div class="txtwork2">
       <h3>I am looking for a great experience.</h3>
       <p>The most important for me, is the team I will integer. I will give my best to meet expectations and to improve the project I will work on.</p>
     </div>
     <div class="imgwork imgwork1">
       <img src="img/livrebasemin.jpg"/>
       </div>
     </div>
     <div class="contentwork" id="lastcontentwork">
       <div class="txtwork">

       </div>

       <div class="imgwork imgwork2">
       </div>
     </div>
   </div>


   <div id="sdr-container">
   <h2>Projects</h2>
    <div class="sep"></div>
   <p>List of projects I am actually working on :</p>
   <a target="_blank" href="https://souvenirsderoute.fr">Souvenirs de route.fr</a><br/><br/>
   <a target="_blank" href="https://jdecalle.fr">Jdecalle.fr</a><br/><br/>
   <a target="_blank" href="https://github.com/karimmorel/translation-quizz">Vocabulary application (work in progress)</a>
   </div>

   <div class="postintro">
   <h3>What I can bring to a team</h3>
   <p>I think I can help to develop maintainable features, to write good quality code following modern patterns and to solve problems related to code issues.<br/>I would like to share with the others what I know and what I can do the best in a company : Doing websites.</p>
 </div>


 <div class="imgblock" id="imgblock2"></div>
 

 <div class="skillblock">
   <h3>Good things about me</h3>
   <p>I am a hardworker, I always want to improve myself by learning everytime I can, and I like to help the others.<br/>Also I can be pretty shy, so I like to be in a positive team to be confident faster.</p>
 </div>


   <div id="contentp">
   <div id="portrait"><div id="textportrait"><h3>Do you like my profile ?</h3><br/><p>I hope you do.<br/>Do not hesitate to contact me for more informations or if you want to interview me. I let you my <a target="_blank" href="public/cv.pdf">CV</a> so you can get more informations about me.</p></div></div>
 </div>
  

   <div class="skillblock">
    <h3>I talked a lot about me</h3>
    <p>It's your time to talk, please have a look to my linkedin account if you want to go further with my profile, and <a href="mailto:job.karimmorel@gmail.com">send me an e-mail !</a></p>
  </div>

 <footer>
  <div id="containfooter">
   <h3>...</h3>
   <div class="sep"></div>
   <div id="footersocial">
    <a href="public/cv.pdf" target="_blank"><i class="fa fa-user" aria-hidden="true"></i></a>
    <a href="https://gitlab.com/karimmorel" target="_blank"><i class="fa fa-gitlab" aria-hidden="true"></i></a>
    <a href="https://github.com/karimmorel/" target="_blank"><i class="fa fa-github" aria-hidden="true"></i></a>
    <a href="https://www.linkedin.com/in/karim-morel-75a18010b" target="_blank"><i class="fa fa-linkedin"></i></a>
    <a href="mailto:job.karimmorel@gmail.com"><i class="fa fa-envelope"></i></a>
  </div>
</div>
<!-- Rediriger vers mon site photo ou vers un site d'un copain ave qui je travaille <div id="footerphoto"><a href="#" target="_blank"><img src="img/photo.png" alt="Logo Photo"/></a></div> -->
</footer> 
</div>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/script.js">
</script>
<script type="text/javascript">
 $(window).load(function(){
   $(".loader").hide("600");
 });
</script>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400" rel="stylesheet">
</body>
</html>

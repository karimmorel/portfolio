<!DOCTYPE html>
<html lang="fr">
    <head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="font/awesome/css/font-awesome.css" />
    <title>Karim Morel | Web Developer</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />


    <meta name="author" content="Karim Morel, Developpeur web" />
    <meta name="copyright" content="Morel" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="ROBOTS" content="INDEX, FOLLOW" />
    <meta name="creation_Date" content="13-03-2016" />

    <meta name="description" content="Karim Morel, developpeur web junior qui est à votre écoute pour un projet de communication digitale ou de création d'un site Internet." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- SOCIAL -->

    <!-- Social: Twitter -->
    <meta name="twitter:card" content="Karim Morel">
    <meta name="twitter:site" content="@MorelKarim">
    <meta name="twitter:creator" content="Karim Morel">
    <meta name="twitter:title" content="Karim Morel - Developpeur Web - Web Créatif">
    <meta name="twitter:description" content="Salut, bienvenue sur mon portfolio, N'hésite pas à me contacter si tu as un projet web.">
    <meta name="twitter:image:src" content="http://www.karimmorel.com/cover/twitter.jpg">

    <!-- Social: Facebook / Open Graph -->
    <meta property="fb:admins" content="karim.morel.5">
    <meta property="og:url" content="http://www.karimmorel.com">
    <meta property="og:type" content="article">
    <meta property="og:title" content="Karim Morel - Developpeur Web - Web Créatif">
    <meta property="og:image" content="http://www.karimmorel.com/cover/twitter.jpg" />
    <meta property="og:description" content="Salut, bienvenue sur mon portfolio, N'hésite pas à me contacter si tu as un projet web.">
    <meta property="og:site_name" content="KarimMorel">
    <meta property="article:author" content="https://www.facebook.com/karim.morel.5">
    <meta property="article:publisher" content="https://www.facebook.com/karim.morel.5"> 
        
    <!-- FONT -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100' rel='stylesheet' type='text/css'>
            
    </head>
    <body>
       <nav>
           <div class="loader"><div id="loadanimcontent"><div class="anim"></div><p>Bonjour.<br/>Passez votre souris sur les formes flottantes qui vont apparaître.</p></div> 
           </div> 
          <div id="navsocial">
        <a href="https://www.facebook.com/karim.morel.5" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://twitter.com/MorelKarim" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/in/karim-morel-75a18010b" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        <a href="mailto:karim-morel@wanadoo.fr"><i class="fa fa-envelope" aria-hidden="true"></i></a>   
       </div>
          <div id="quitnav">
           <i class="fa fa-times" aria-hidden="true"></i>
       </div>
           <ul>
               <li class="actif lileft"><a href="index.php">MES COMPETENCES</a></li>
               <li class="liright"><a href="relation.php">VOTRE PROJET</a></li>
           </ul>
           <div class="sep"></div>
       </nav>
       <div id="contentmenubutton">
       <div id="menubutton"><i class="fa fa-bars"></i></div>
        </div>
    
       <div id="top">
       </div>
       <div id="content">
       <div id="intro">
               <h2>Faites disparaître une bulle.</h2>
               <div class="sep"></div>
        </div>
              <div id="postintro">
               <h3>À votre disposition.</h3>
               <p>Vous souhaitez développer votre activité, communiquer sur un projet, étendre votre communication, ou vous cherchez tout simplement <strong>un développeur web</strong> pour votre entreprise : N'hésitez pas à venir me parler de votre proposition ou <strong>à venir me partager vos différentes expériences.</strong></p>
           </div>
           <div id="contentp">
               <div id="portrait"><div id="textportrait"><h3>Découverte.</h3><br/><p>Apprendre, innover, me dépasser.<br/>Rester créatif et original sont des activités à plein temps. J'aime voir de nouveaux projets s'établir, de nouvelles innovations émerger et participer à ces différentes aventures. Laissez-moi apporter ma pierre à votre projet et nous pourrions avancer ensemble sur des travaux qui vous tiennent à coeur.</p></div></div>
           </div>
           
           <div id="skillblock">
            <h3>Un café ?</h3>
               <p>J'aime voir ce que les personnes autour de moi sont capable de faire. Dépêchez-vous ! Discutons ensemble de vos travaux et vos projets avec moi histoire de partager nos différents points de vue. Vous êtes à un <strong><a href="mailto:karim-morel@wanadoo.fr" title="Laissez moi un message !">mail</a></strong> de lancer un échange.</p>
           </div>
           <div class="imgblock" id="imgblock2"></div>
           <footer>
              <div id="containfooter">
               <h3>ME CONTACTER</h3>
               <div class="sep"></div>
               <div id="footersocial">
        <a href="https://www.facebook.com/karim.morel.5" target="_blank"><i class="fa fa-facebook-square"></i></a>
        <a href="https://twitter.com/MorelKarim" target="_blank"><i class="fa fa-twitter-square"></i></a>
        <a href="https://www.linkedin.com/in/karim-morel-75a18010b" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        <a href="mailto:karim-morel@wanadoo.fr"><i class="fa fa-envelope-square"></i></a>   
       </div>
               </div>
               <div id="footerphoto"><a href="https://www.facebook.com/melinaperreauphotographies/?fref=ts" target="_blank"><img src="img/photo.png"/></a></div>
           </footer> 
           </div>
                 <script type="text/javascript" src="js/jquery.js"></script> 
<script type="text/javascript">
        $(window).load(function(){
            $(".loader").hide("600"); 
        })
    </script>
    <script type="text/javascript" src="js/script.js"></script>
    </body> 
    
</html>